import 'package:flutter/material.dart';

main() {
  runApp(
    MaterialApp(
      home: TabView(),
    ),
  );
}

class TabView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return TabViewState();
  }
}

class TabViewState extends State<TabView> with TickerProviderStateMixin {
  TabController controller;

  @override
  void initState() {
    super.initState();
    controller = TabController(length: 3, vsync: this);
  }

  @override
  void dispose() {
    super.dispose();
    controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.teal,
        title: Text('TabView'),
        bottom: TabBar(
          controller: controller,
          tabs: <Tab>[
            Tab(
              icon: Icon(Icons.looks_one),
            ),
            Tab(
              icon: Icon(Icons.looks_two),
            ),
            Tab(
              icon: Icon(Icons.looks_3),
            ),
          ],
        ),
      ),
      body: Material(
        color: Colors.white70,
        child: TabBarView(
          controller: controller,
          children: <Widget>[
            Center(
              child: Text('Tab One'),
            ),
            Center(
              child: Text('Tab Two'),
            ),
            Center(
              child: Text('Tab Three'),
            ),
          ],
        ),
      ),
      bottomNavigationBar: Material(
        color: Colors.teal,
        child: TabBar(
          controller: controller,
          indicatorColor: Colors.white,
          tabs: <Tab>[
            Tab(
              icon: Icon(Icons.looks_one),
            ),
            Tab(
              icon: Icon(Icons.looks_two),
            ),
            Tab(
              icon: Icon(Icons.looks_3),
            ),
          ],
        ),
      ),
    );
  }
}
